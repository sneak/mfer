module git.eeqj.de/sneak/mfer

go 1.17

require (
	github.com/apex/log v1.9.0
	github.com/davecgh/go-spew v1.1.1
	github.com/pterm/pterm v0.12.35
	github.com/spf13/afero v1.8.0
	github.com/stretchr/testify v1.8.1
	github.com/urfave/cli/v2 v2.23.6
	google.golang.org/protobuf v1.28.1

)

require (
	github.com/atomicgo/cursor v0.0.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/gookit/color v1.4.2 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.0.0-20211013075003-97ac67df715c // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
