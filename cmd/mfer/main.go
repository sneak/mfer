package main

import (
	"os"

	"git.eeqj.de/sneak/mfer/internal/cli"
)

var (
	Appname string = "mfer"
	Version string
	Gitrev  string
)

func main() {
	os.Exit(cli.Run(Appname, Version, Gitrev))
}
