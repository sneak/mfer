package bork

import (
	"errors"
	"fmt"
)

var (
	ErrMissingMagic  = errors.New("missing magic bytes in file")
	ErrFileTruncated = errors.New("file/stream is truncated abnormally")
)

func Newf(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}
