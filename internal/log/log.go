package log

import (
	"fmt"
	"runtime"

	"github.com/apex/log"
	acli "github.com/apex/log/handlers/cli"
	"github.com/davecgh/go-spew/spew"
	"github.com/pterm/pterm"
)

type Level = log.Level

func DisableStyling() {
	pterm.DisableColor()
	pterm.DisableStyling()
	pterm.Debug.Prefix.Text = ""
	pterm.Info.Prefix.Text = ""
	pterm.Success.Prefix.Text = ""
	pterm.Warning.Prefix.Text = ""
	pterm.Error.Prefix.Text = ""
	pterm.Fatal.Prefix.Text = ""
}

func Init() {
	log.SetHandler(acli.Default)
	log.SetLevel(log.InfoLevel)
}

func Debugf(format string, args ...interface{}) {
	DebugReal(fmt.Sprintf(format, args...), 2)
}

func Debug(arg string) {
	DebugReal(arg, 2)
}

func DebugReal(arg string, cs int) {
	_, callerFile, callerLine, ok := runtime.Caller(cs)
	if !ok {
		return
	}
	tag := fmt.Sprintf("%s:%d: ", callerFile, callerLine)
	log.Debug(tag + arg)
}

func Dump(args ...interface{}) {
	DebugReal(spew.Sdump(args...), 2)
}

func EnableDebugLogging() {
	SetLevel(log.DebugLevel)
}

func VerbosityStepsToLogLevel(l int) log.Level {
	switch l {
	case 1:
		return log.WarnLevel
	case 2:
		return log.InfoLevel
	case 3:
		return log.DebugLevel
	}
	return log.ErrorLevel
}

func SetLevelFromVerbosity(l int) {
	SetLevel(VerbosityStepsToLogLevel(l))
}

func SetLevel(arg log.Level) {
	log.SetLevel(arg)
}

func GetLogger() *log.Logger {
	if logger, ok := log.Log.(*log.Logger); ok {
		return logger
	}
	panic("unable to get logger")
}

func GetLevel() log.Level {
	return GetLogger().Level
}

func WithError(e error) *log.Entry {
	return GetLogger().WithError(e)
}
