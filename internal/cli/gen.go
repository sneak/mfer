package cli

import (
	"bytes"
	"path/filepath"

	"git.eeqj.de/sneak/mfer/internal/log"
	"git.eeqj.de/sneak/mfer/mfer"
	"github.com/urfave/cli/v2"
)

func (mfa *CLIApp) generateManifestOperation(ctx *cli.Context) error {
	log.Debug("generateManifestOperation()")
	myArgs := ctx.Args()
	log.Dump(myArgs)

	opts := &mfer.ManifestScanOptions{
		IgnoreDotfiles: ctx.Bool("IgnoreDotfiles"),
		FollowSymLinks: ctx.Bool("FollowSymLinks"),
	}
	paths := make([]string, ctx.Args().Len()-1)
	for i := 0; i < ctx.Args().Len(); i++ {
		ap, err := filepath.Abs(ctx.Args().Get(i))
		if err != nil {
			return err
		}
		log.Dump(ap)
		paths = append(paths, ap)
	}
	mf, err := mfer.NewFromPaths(opts, paths...)
	if err != nil {
		panic(err)
	}
	mf.WithContext(ctx.Context)

	log.Dump(mf)

	err = mf.Scan()
	if err != nil {
		return err
	}

	buf := new(bytes.Buffer)

	err = mf.WriteTo(buf)
	if err != nil {
		return err
	}

	dat := buf.Bytes()

	log.Dump(dat)
	return nil
}
