package cli

import (
	"os"
)

var NO_COLOR bool

func init() {
	NO_COLOR = false
	if _, exists := os.LookupEnv("NO_COLOR"); exists {
		NO_COLOR = true
	}
}

func Run(Appname, Version, Gitrev string) int {
	m := &CLIApp{}
	m.appname = Appname
	m.version = Version
	m.gitrev = Gitrev
	m.exitCode = 0

	m.run()
	return m.exitCode
}
