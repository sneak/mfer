package mfer

import (
	"io"
	"os"
)

func (m *manifest) WriteToFile(path string) error {
	// FIXME refuse to overwrite without -f if file exists

	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	return m.WriteTo(f)
}

func (m *manifest) WriteTo(output io.Writer) error {
	if m.pbOuter == nil {
		err := m.generate()
		if err != nil {
			return err
		}
	}

	_, err := output.Write(m.output.Bytes())
	if err != nil {
		return err
	}
	return nil
}
