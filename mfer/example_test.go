package mfer

import (
	"bytes"
	"testing"

	"git.eeqj.de/sneak/mfer/internal/log"
	"github.com/stretchr/testify/assert"
)

func TestAPIExample(t *testing.T) {
	// read from filesystem
	m, err := NewFromFS(&ManifestScanOptions{
		IgnoreDotfiles: true,
	}, big)
	assert.Nil(t, err)
	assert.NotNil(t, m)

	// scan for files
	m.Scan()

	// serialize
	var buf bytes.Buffer
	m.WriteTo(&buf)

	// show serialized
	log.Dump(buf.Bytes())

	// do it again
	var buf2 bytes.Buffer
	m.WriteTo(&buf2)

	// should be same!
	assert.True(t, bytes.Equal(buf.Bytes(), buf2.Bytes()))

	// deserialize
	m2, err := NewFromProto(&buf)
	assert.Nil(t, err)
	assert.NotNil(t, m2)

	log.Dump(m2)
}
