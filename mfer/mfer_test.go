package mfer

import (
	"bytes"
	"fmt"
	"testing"

	"git.eeqj.de/sneak/mfer/internal/log"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

// Add those variables as well
var (
	existingFolder = "./testdata/a-folder-that-exists"
)

var (
	af  *afero.Afero = &afero.Afero{Fs: afero.NewMemMapFs()}
	big *afero.Afero = &afero.Afero{Fs: afero.NewMemMapFs()}
)

func init() {
	log.EnableDebugLogging()

	// create test files and directories
	af.MkdirAll("/a/b/c", 0o755)
	af.MkdirAll("/.hidden", 0o755)
	af.WriteFile("/a/b/c/hello.txt", []byte("hello world\n\n\n\n"), 0o755)
	af.WriteFile("/a/b/c/hello2.txt", []byte("hello world\n\n\n\n"), 0o755)
	af.WriteFile("/.hidden/hello.txt", []byte("hello world\n"), 0o755)
	af.WriteFile("/.hidden/hello2.txt", []byte("hello world\n"), 0o755)

	big.MkdirAll("/home/user/Library", 0o755)
	for i := range [25]int{} {
		big.WriteFile(fmt.Sprintf("/home/user/Library/hello%d.txt", i), []byte("hello world\n"), 0o755)
	}
}

func TestPathHiddenFunc(t *testing.T) {
	assert.False(t, pathIsHidden("/a/b/c/hello.txt"))
	assert.True(t, pathIsHidden("/a/b/c/.hello.txt"))
	assert.True(t, pathIsHidden("/a/.b/c/hello.txt"))
	assert.True(t, pathIsHidden("/.a/b/c/hello.txt"))
	assert.False(t, pathIsHidden("./a/b/c/hello.txt"))
}

func TestManifestGenerationOne(t *testing.T) {
	m, err := NewFromFS(&ManifestScanOptions{
		IgnoreDotfiles: true,
	}, af)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	m.Scan()
	assert.Equal(t, int64(2), m.GetFileCount())
	assert.Equal(t, int64(30), m.GetTotalFileSize())
}

func TestManifestGenerationTwo(t *testing.T) {
	m, err := NewFromFS(&ManifestScanOptions{
		IgnoreDotfiles: false,
	}, af)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	m.Scan()
	assert.Equal(t, int64(4), m.GetFileCount())
	assert.Equal(t, int64(54), m.GetTotalFileSize())
	err = m.generate()
	assert.Nil(t, err)
	var buf bytes.Buffer
	err = m.WriteTo(&buf)
	assert.Nil(t, err)
	log.Dump(buf.Bytes())
}
