#!/bin/bash
#
if [[ ! -z "$DRONE_COMMIT_SHA" ]]; then
    echo "${DRONE_COMMIT_SHA:0:7}"
    exit 0
fi

if [[ ! -z "$GITREV" ]]; then
    echo $GITREV
else
    git describe --always --dirty=-dirty
fi
